# Holographic Video Monitor Electronic Supplements

This repository contains SolidWorks CAD files for the Mark V Holographic Video Monitor, specifically models of circuit boards and electronic components as needed for the complete system assembly ([Full Monitor System.SLDASM](AcrylicChassis/Full\ Monitor\ System.SLDASM) in AcrylicChassis). Files may periodically change without any obvious differences as rebuild occur during assembly.

Omitted from this repository are several pre-made objects obtained from manufacturers or public CAD repositories which can't be included in our release. Obtain the following models in order to view the complete assembly:
* A full model of an Arduino Mega 2560 (We used [this](http://www.3dcontentcentral.com/download-model.aspx?catalogid=171&id=455564) one from Dassault Systèmes 3d ContentCentral™)
* A large rotary potentiometer similar to [this](http://www.digikey.com/short/qczh5p)
* A rectangular, finned heatsink like [this](http://www.digikey.com/short/qczh1w) one
* An 80mm Case fan (shoudn't be hard to find on [Digi-Key](http://www.digikey.com/))
* A Mini-Circuits™ HELA-10 amplifier chip (Good luck. I don't know where we found the one we have)
* A through-hole right-angle DVI receptacle ([this](https://www.digikey.com/short/qc43bz) kind)
* Three types of SMA connectors:
  * A board edge female connector (along the lines of [this](http://www.digikey.com/short/qczhd0))
  * A right-angle female-female connector (we found a model of [this](http://www.amphenolrf.com/132339.html) one)
  * A square base through-hole female connector (like [this](http://www.digikey.com/short/qczh03))
* The following Molex™ connectors
  * [Shrouded 2-pin Male Board connector](http://www.digikey.com/short/qczhvt)
  * [Shrouded 4-pin Male Board connector](http://www.digikey.com/short/qczhv8)

# License

All files in this repository are Copyright 2015-2019 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([http://www.gnu.org/licenses/gpl-3.0.en.html](http://www.gnu.org/licenses/gpl-3.0.en.html))

![GPL v3 Logo](http://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
