"DVI_breakoutBoardLength" = 1.838in
"DVI_breakoutBoardWidth" = 1.050in
"DVI_breakoutBoardThick" = .050in


'ten pin connector
"tenPinConnectorExtLength" = 0.815in
"tenPinConnectorExtHeight" = 0.345in
"tenPinConnectorExtWidth" = 0.345in
"tenPinConnectorIntWidth" = 0.258in
"tenPinConnectorIntLength" = 0.712in
"tenPinConnectorIntHeight" = 0.270in
"tenPinConnectorPinWidth" = 0.030in
"tenPinConnectorPinHeight" = "tenPinConnectorIntHeight"
"tenPinConnectorPinDist" = .07in 'space between pins

"smaDistToEdge" = 0.12in 'distance from closest pin edge on top to edge of board
"smaDistBetween" = 0.420in 'distance between centers of connectors




